import socket
import threading

class client:
    def __init__(self, id, conn, status):
        self.id = id
        self.conn = conn
        self.status = status

class job:
    def __init__(self, user_id, message):
        self.user_id = user_id
        self.message = message

HEADER = 64
FORMAT = 'utf-8'
PORT = 5050
HOST = socket.gethostbyname(socket.gethostname())
ADDR = (HOST, PORT)
running = True

TYPE_WORKER = "WORKER"
TYPE_USER = "USER"

STATUS_READY = "READY"
STATUS_BUSY = "BUSY"

DISCONNECT_MSG = "!DISCONNECT"
WORKER_DISCONNECT_MSG = "!DISC_WORKER"
WORKER_STATUS_MSG = "!WORKERS"
ENCODE_MSG = "!ENCODE"
DECODE_MSG = "!DECODE"
HELP_MSG = "!HELP"

HELP_COMMANDS = """Commands:
!WORKERS = Get list of worker
!DISC_WORKER <Worker_id> = Disconnect a worker from server
!ENCODE <Offset> <Message> = Encode a message with caesar's cipher
!DECODE <Offset> <Message> = Decode a message with caesar's cipher
!DISCONNECT = Disconnect user
"""

workers = {}
workers_id = 0
users = {}
users_id = 0
round_robin = 0

queue_jobs = []

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print("[STARTING] server is starting...")
server.bind(ADDR)

def send(conn, message):
    message = message.encode(FORMAT)
    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b' ' * (HEADER - len(send_length))
    conn.send(send_length)
    conn.send(message)

def receive(conn):
    msg_length = conn.recv(HEADER).decode(FORMAT)
    if(msg_length):
        msg_length = int(msg_length)
        msg = conn.recv(msg_length).decode(FORMAT)
        return msg
    return None

def allocate_worker():
    global running
    global round_robin
    while running:
        job_inst = None
        if(len(queue_jobs) != 0):
            job_inst = queue_jobs.pop(0)
        if(job_inst):
            if(len(workers) == 0):
                send(users[job_inst.user_id], "[SERVER RESPONSE] ERROR: no active worker")
                continue
            num = round_robin % len(workers)
            worker_key = list(workers.keys())[num]
            worker = workers[worker_key]
            round_robin += 1
            send(worker.conn, job_inst.message)
            send(worker.conn, job_inst.user_id)
            print(f"[BALANCING] job sent to Worker {worker_key}")
            workers[worker_key].status = STATUS_BUSY

def listen_worker(conn, addr, worker_id):
    connected = True
    while connected:
        response = receive(conn)
        if(response == DISCONNECT_MSG):
            connected = False
            break
        user_id = receive(conn)
        worker_status = receive(conn)
        send(users[user_id].conn, f"[WORKER {worker_id}] {response}")
        workers[worker_id].status = worker_status
    conn.close()
    
def listen_user(conn, addr, user_id):
    connected = True
    while connected:
        request = receive(conn)
        if(not request):
            send(conn, "[SERVER RESPONSE] ERROR: Unknown Command, use !HELP for list of commands")
            continue
        command = request.split()[0]
        print(f"[{addr}] {request}")
        if(command == DISCONNECT_MSG):
            connected = False
            users.pop(user_id)
            response = "Terminating connection..."
        elif(command == WORKER_DISCONNECT_MSG):
            try:
                num = request.split()[1]
                if(not workers[num].status == STATUS_BUSY):
                    send(workers[num].conn, DISCONNECT_MSG)
                    workers.pop(num)
                    response = "terminating worker connection..."
                else:
                    response = "ERROR: Worker still running instance"
            except IndexError:
                response = "ERROR: Missing Argument"
            except KeyError:
                response = "ERROR: Worker not found"
        elif(command == WORKER_STATUS_MSG):
            response = "WORKERS:\n"
            for keys in list(workers.keys()):
                worker = workers[keys]
                response += f"Worker id : {worker.id}; Status : {worker.status}\n"
        elif(command == HELP_MSG):
            response = HELP_COMMANDS
        else:
            queue_jobs.append(job(user_id, request))
            continue
        send(conn, f"[SERVER RESPONSE] {response}")
    conn.close()

def start():
    global running
    global workers_id
    global users_id
    server.listen()
    print(f"[LISTENING] server listening on {ADDR}")
    thread = threading.Thread(target=allocate_worker, args=())
    thread.start()
    while running:
        conn, addr = server.accept()
        data = receive(conn)
        if(data == TYPE_WORKER):
            print(f"[WORKER] Worker {addr} is connecting")
            thread = threading.Thread(target=listen_worker, args=(conn, addr, str(workers_id)))
            thread.start()
            workers[str(workers_id)] = client(str(workers_id), conn, STATUS_READY)
            workers_id += 1
        elif(data == TYPE_USER):
            print(f"[CONNECTED] User {addr} is connecting")
            thread = threading.Thread(target=listen_user, args=(conn, addr, str(users_id)))
            thread.start()
            users[str(users_id)] = client(str(users_id), conn, "NONE")
            users_id += 1
        else:
            continue
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 2}")
    server.close()

if __name__ == "__main__":
    start()