import socket
import threading
import argparse
import string
import time

class job:
    def __init__(self, user_id, message):
        self.user_id = user_id
        self.message = message

parser = argparse.ArgumentParser()
parser.add_argument("TYPE", help="WORKER for worker node, USER for user node")
parser.add_argument("HOST", help="server HOST address")
parser.add_argument("PORT", help="server PORT")
args = parser.parse_args()
print(args.TYPE, args.HOST, args.PORT)

HEADER = 64
FORMAT = 'utf-8'
PORT = int(args.PORT)
SERVER = args.HOST
ADDR = (SERVER, PORT)
CLIENT_ATTR = {
    "ADDR" : ADDR,
    "TYPE" : args.TYPE,
    "HOST" : args.HOST,
    "PORT" : args.PORT
}
running = True

TYPE_WORKER = "WORKER"
TYPE_USER = "USER"

DISCONNECT_MSG = "!DISCONNECT"
ENCODE_MSG = "!ENCODE"
DECODE_MSG = "!DECODE"

STATUS_READY = "READY"
STATUS_BUSY = "BUSY"

queue_jobs = []

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print(f"[CONNECTING] connecting to {ADDR}")
client.connect(ADDR)

def send(message):
    message = message.encode(FORMAT)
    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b' ' * (HEADER - len(send_length))
    client.send(send_length)
    client.send(message)

def receive():
    msg_length = int(client.recv(HEADER).decode(FORMAT))
    if(msg_length):
        msg_length = int(msg_length)
        msg = client.recv(msg_length).decode(FORMAT)
        return msg
    return None

def caesar_encode(offset, message):
    time.sleep(3)
    if(offset <= 0 or offset >= 26):
        return "ERROR: Offset must be between 1-25"
    result = ""
    for char in message:
        if(char.islower()):
            result += chr((((ord(char) - ord('a')) + offset) % 26) + ord('a'))
        elif(char.isupper()):
            result += chr((((ord(char) - ord('A')) + offset) % 26) + ord('A'))
        else:
            result += char
    return result

def caesar_decode(offset, message):
    return caesar_encode(26 - offset, message)

def start_worker():
    global running
    thread = threading.Thread(target=run_jobs, args=())
    thread.start()
    while running:
        message = receive()
        if(message == DISCONNECT_MSG):
            send(DISCONNECT_MSG)
            running = False
            break
        user_id = receive()
        print(f"[WORKER] message received : {message}; from : {user_id}")
        queue_jobs.append(job(user_id, message))

def run_jobs():
    global running
    while running:
        job_inst = None
        if(len(queue_jobs) != 0):
            job_inst = queue_jobs.pop(0)
        if(job_inst):
            message_list = job_inst.message.split()
            command = message_list[0]
            result = "ERROR: Unknown Command, use !HELP for list of commands"
            if(command == ENCODE_MSG):
                try:
                    message = ""
                    for obj in message_list[2:]:
                        message += f"{obj} "
                    message = message.strip()
                    result = caesar_encode(int(message_list[1]), message)
                except IndexError:
                    result = "ERROR: Missing argument"
                except ValueError:
                    result = "ERROR: OFFSET is not number"
                except:
                    result = "ERROR: Something went wrong"
            if(command == DECODE_MSG):
                try:
                    message = ""
                    for obj in message_list[2:]:
                        message += f"{obj} "
                    message = message.strip()
                    result = caesar_decode(int(message_list[1]), message)
                except IndexError:
                    result = "ERROR: Missing argument"
                except ValueError:
                    result = "ERROR: OFFSET is not number"
                except:
                    result = "ERROR: Something went wrong"
            send(result)
            send(job_inst.user_id)
            if(len(queue_jobs) != 0):
                send(STATUS_BUSY)
            else:
                send(STATUS_READY)

    
def start_user():
    global running
    while running:
        message = input(">>> ")
        send(message)
        if(message == DISCONNECT_MSG):
            running = False
            break
        print(receive())

if __name__ == "__main__":
    send(args.TYPE)
    if(args.TYPE == TYPE_WORKER):
        start_worker()
    elif(args.TYPE == TYPE_USER):
        start_user()
